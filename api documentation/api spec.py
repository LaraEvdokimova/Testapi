API Documentation

API Method: /api/v2/payments/{payment_id}/sep0031/info

Description
Returns information about a payment method for a given asset.

Parameters
•	payment_id (required): A string parameter specifying the ID of the payment.
•	countries (required): A string parameter specifying a list of countries where the payment method is available. 
The value "WWC" might indicate "worldwide coverage".
•	asset (required): A string parameter that specifies the asset for the payment method. 
The value "ATUSD" indicates "USD asset".

Response
•	HTTP Status Code: 200 OK
•	Body: Information related to the payment method.

Error Responses
•	HTTP Status Code: 400 Bad Request
•	Body: An error message indicating the reason for the failure.
