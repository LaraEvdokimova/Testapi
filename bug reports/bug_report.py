# BUG REPORT. Summary: API does not return expected error message for invalid countries parameter.

# Steps to Reproduce:
1.	Send a GET request to the API endpoint
https://payments-api.armenotech.dev/api/v2/payments/edeab824-178e-4fd7-9bf0-bd88a6fd114a/sep0031/info with an invalid countries parameter (e.g. AAA)
2.	Verify the response status code and body

# Expected Result: 
The API should return a response with status code 400 Bad Request and an error message indicating that the countries parameter is invalid.

# Actual Result: 
The API returns a response with status code 200 OK and no error message, indicating that the request was successful.

# Additional Information:
• This issue was observed on May 1, 2023 at 10:00 AM EST.
• The asset parameter was set to a valid value (ATUSD) in the request.
• The issue was reproducible on multiple attempts.
• The API documentation does not specify any error codes or messages for invalid countries parameters.
• A similar test for an invalid asset parameter was successful, indicating that the test setup and execution are correct.

# Impact: 
This issue may cause confusion and errors for users who are attempting to use the API with an invalid countries parameter. 
It may also result in unexpected behavior or errors in downstream systems that rely on the API response.

# Priority: High. 
This issue should be addressed as soon as possible, as it may cause confusion and errors for users and could lead to unexpected behavior 
or errors in downstream systems.

# Severity: High. 
The severity of this issue is high, as it prevents the API from properly handling invalid countries parameters and 
may result in incorrect or unexpected behavior.
