import requests

# Set up the request parameters
url = 'https://payments-api.armenotech.dev/api/v2/payments/edeab824-178e-4fd7-9bf0-bd88a6fd114a/sep0031/info'
params = {'countries': 'WWC', 'asset': 'ATUSD'}

# Send the request and store the response
response = requests.get(url, params=params)

# Verify that the response status code is 200 OK
assert response.status_code == 200, f"Expected status code 200 but got {response.status_code}"

# Verify that the response body contains expected data
expected_data = {"receive":{"ATBRL":{"fields":{"r:c131a779-b4ce-4100-a421-6e51ebfb5ed6":{"guid":"r:c131a779-b4ce-4100-a421-6e51ebfb5ed6","fee_fixed_min":0,"fee_percent_min":0,"fee_fixed_max":1,"fee_percent_max":16,"customer_fee_fixed":1,"customer_fee_percent":10,"delivery_digits":2,"asset_digits":2,"min_amount":13.45,"max_amount":30865.3,"rate":0.18,"direction":"out","label":"Some label","logo_url":"test_logo","mobile_logo_url":"mobile_logo","payment_group":"BANKCARD","payment_group_name":"Bank card","delivery_currency":"iso4217:EUR","country_code":"WWC","customer_kind":"both","fields":{}}}}}}

assert response.json() == expected_data, "Response data does not match expected data"

# This happy path test verifies that the API returns a successful response when valid parameters are provided.
# This test send a GET request to the specified URL with the parameters specified in params. 
# The test verifies that the response status code is 200 OK and that the response body matches the expected data. 


