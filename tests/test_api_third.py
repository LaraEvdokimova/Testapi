import requests

def test_correct_payment_method_info():
    # Set up the request parameters
    url = 'https://payments-api.armenotech.dev/api/v2/payments/edeab824-178e-4fd7-9bf0-bd88a6fd114a/sep0031/info'
    params = {
        'countries': 'WWC',
        'asset_type': 'ATUSD'
    }

    # Send the request and get the response
    response = requests.get(url, params=params)

    # Verify that the response is successful (HTTP status code 200)
    assert response.status_code == 200

    # Verify that the response contains the correct payment method information for the given asset
    payment_method_info = response.json()
    assert payment_method_info['asset_type'] == 'USD'
    assert payment_method_info['type'] == 'SEP-0031'
    assert payment_method_info['countries'] == ['WWC']

# Positive Test that verifies that the API returns the correct payment method information for a valid asset parameter.
# This test uses the requests library to send a GET request to the specified URL with the given parameters. 
# It then checks that the HTTP status code of the response is 200, indicating a successful response. 
# If the status code is not 200, the test will fail. 
# The test then extracts the payment method information from the response using the json() method, and 
# checks that the asset, type, and countries fields match the expected values for the given asset. 
# If any of these checks fail, the test will fail.
# Make sure to update all relevant assertions in the test to use the correct field names.

