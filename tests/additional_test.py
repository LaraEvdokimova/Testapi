import requests

# Set up the request parameters with an invalid countries parameter
url = 'https://payments-api.armenotech.dev/api/v2/payments/edeab824-178e-4fd7-9bf0-bd88a6fd114a/sep0031/info'
params = {'countries': 'AAA', 'asset': 'ATUSD'}

# Send the request and store the response
response = requests.get(url, params=params)

# Verify that the response status code is 400 Bad Request
assert response.status_code == 400, f"Expected status code 400 but got {response.status_code}"

# Verify that the response body contains an error message
assert 'error' in response.json(), "Expected error message in response body"

# This test verifies that the API returns an error message when an invalid countries parameter is provided.
# This test send a GET request to the specified URL with the parameters specified in params. 
# It verifies that the response status code is 400 Bad Request and that the response body contains an error message 
# When an invalid countries parameter is provided.



