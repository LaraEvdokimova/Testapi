import unittest
import requests

class TestHappyPath(unittest.TestCase):
    
    def test_valid_parameters(self):
        # Set up the request parameters
        url = 'https://payments-api.armenotech.dev/api/v2/payments/edeab824-178e-4fd7-9bf0-bd88a6fd114a/sep0031/info'
        params = {'countries': 'WWC', 'asset': 'ATUSD'}

        # Send the request and store the response
        response = requests.get(url, params=params)

        # Verify that the response status code is 200 OK
        self.assertEqual(response.status_code, 200)

        # Verify that the response body contains expected data
        expected_data = {"receive":{"ATBRL":{"fields":{"r:c131a779-b4ce-4100-a421-6e51ebfb5ed6":{"guid":"r:c131a779-b4ce-4100-a421-6e51ebfb5ed6","fee_fixed_min":0,"fee_percent_min":0,"fee_fixed_max":1,"fee_percent_max":16,"customer_fee_fixed":1,"customer_fee_percent":10,"delivery_digits":2,"asset_digits":2,"min_amount":13.45,"max_amount":30865.3,"rate":0.18,"direction":"out","label":"Some label","logo_url":"test_logo","mobile_logo_url":"mobile_logo","payment_group":"BANKCARD","payment_group_name":"Bank card","delivery_currency":"iso4217:EUR","country_code":"WWC","customer_kind":"both","fields":{}}}}}}
        self.assertEqual(response.json(), expected_data)

if __name__ == '__main__':
    unittest.main()

# We use the unittest framework to define test classes and methods. 
# Each test method sends a request to the API and verifies the response status code and body using assertions. 
# You can run these tests using the python -m unittest command in the terminal.

