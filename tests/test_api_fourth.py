import requests

# Set up the request parameters
url = 'https://payments-api.armenotech.dev/api/v2/payments/edeab824-178e-4fd7-9bf0-bd88a6fd114a/sep0031/info'
params = {'countries': 'WWC', 'asset': 'XYZ'}

# Send the request and store the response
response = requests.get(url, params=params)

# Verify that the response status code is 400 Bad Request
assert response.status_code == 400, f"Expected status code 400 but got {response.status_code}"

# Verify that the response body contains an error message
assert 'error' in response.json(), "Expected error message in response body"

# Negative test that verifies that the API returns an error message when an invalid asset parameter is passed.
# This test sends a GET request to the specified URL with the parameters specified in params. 
# It then verifies that the response status code is 400 Bad Request, indicating that the API returned an error. 
# Finally, it verifies that the response body contains an error message by checking for the presence of the string "error" in the JSON response.

