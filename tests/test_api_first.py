import requests

def test_api():
    url = 'https://payments-api.armenotech.dev/api/v2/payments/edeab824-178e-4fd7-9bf0-bd88a6fd114a/sep0031/info'
    params = {
        'countries': 'WWC',
        'asset': 'ATUSD'
    }
    response = requests.get(url, params=params)
    assert response.status_code == 200
    assert 'receive' in response.json()
    assert 'ATBRL' in response.json()['receive']
   

# add more assertions as needed
# In this test_api, we are using the cy.request() command to make a GET request to the API endpoint with the countries and asset parameters 
# set to valid values. Then use the then() function to assert that the response status is 200 and that the response body contains 
# the expected properties. I can add more positive test cases by varying the values of the countries and asset parameters and asserting that 
# the response is correct. So I need to modify it to fit your specific use case.
