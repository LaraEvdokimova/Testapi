import unittest
import requests

class TestAdditional(unittest.TestCase):
    
    def test_invalid_countries_parameter(self):
        # Set up the request parameters with an invalid countries parameter
        url = 'https://payments-api.armenotech.dev/api/v2/payments/edeab824-178e-4fd7-9bf0-bd88a6fd114a/sep0031/info'
        params = {'countries': 'AAA', 'asset': 'ATUSD'}

        # Send the request and store the response
        response = requests.get(url, params=params)

        # Verify that the response status code is 400 Bad Request
        self.assertEqual(response.status_code, 400)

        # Verify that the response body contains an error message
        self.assertIn('error', response.json())

if __name__ == '__main__':
    unittest.main()
    
# The test uses the unittest framework to define test classes and methods. 
# Each test method sends a request to the API and verifies the response status code and body using assertions. 
# You can run these tests using the python -m unittest command in the terminal.

