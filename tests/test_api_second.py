import requests

def test_successful_response():
    # Set up the request parameters
    url = 'https://payments-api.armenotech.dev/api/v2/payments/edeab824-178e-4fd7-9bf0-bd88a6fd114a/sep0031/info'
    params = {
        'countries': 'WWC',
        'asset': 'ATUSD'
    }

    # Send the request and get the response
    response = requests.get(url, params=params)

    # Verify that the response is successful (HTTP status code 200)
    assert response.status_code == 200
    
# Positive Test that verifies that the API returns a successful response with valid parameters.
# This test uses the requests library to send a GET request to the specified URL with the given parameters. 
# It then checks that the HTTP status code of the response is 200, indicating a successful response. 
# If the status code is not 200, the test will fail.