# Provide script to run tests from Console. Provide any test run results with HTML 

# Script that you can use to run all the tests from the console
import unittest
from test_api_first import TestAPIFirst
from test_api_second import TestAPITwo
from test_api_third import TestAPIThree
from test_api_fourth import TestAPIFourth
from test_api_fifth import TestAPIFifth

if __name__ == '__main__':
    # Create a test suite
    suite = unittest.TestSuite()

    # Add the tests to the suite
    suite.addTest(unittest.makeSuite(TestAPIFirst))
    suite.addTest(unittest.makeSuite(TestAPITwo))
    suite.addTest(unittest.makeSuite(TestAPIThree))
    suite.addTest(unittest.makeSuite(TestAPIFourth))
    suite.addTest(unittest.makeSuite(TestAPIFifth))

    # Run the test suite and output the results to the console
    runner = unittest.TextTestRunner()
    runner.run(suite)

# This script imports all the test classes and adds them to a test suite, which is then run using the TextTestRunner class. 
# You can run this script from the console using the command python <script_name>.py.

# To generate an HTML report of the test results, you can use the HTMLTestRunner package. 
# You can modify the previous script to use HTMLTestRunner

import unittest
import HtmlTestRunner
from test_api_first import TestAPIFirst
from test_api_second import TestAPITwo
from test_api_third import TestAPIThree
from test_api_fourth import TestAPIFourth
from test_api_fifth import TestAPIFifth

if __name__ == '__main__':
    # Create a test suite
    suite = unittest.TestSuite()

    # Add the tests to the suite
    suite.addTest(unittest.makeSuite(TestAPIFirst))
    suite.addTest(unittest.makeSuite(TestAPITwo))
    suite.addTest(unittest.makeSuite(TestAPIThree))
    suite.addTest(unittest.makeSuite(TestAPIFourth))
    suite.addTest(unittest.makeSuite(TestAPIFifth))

    # Run the test suite and output the results to an HTML report
    runner = HtmlTestRunner.HTMLTestRunner(output='test-reports/')
    runner.run(suite)

# This modified script uses HTMLTestRunner to generate an HTML report of the test results, which is saved in the test-reports/ directory. 
# You can open this report in a web browser to view the results.

# To run the tests from console, you can use a testing framework like Mocha or Jest and write test cases for each scenario. 
# You can then run the tests using the command line interface and generate HTML test results 
# Using a reporting tool like Mochawesome or Jest HTML Reporter.
# Mochawesome. Mochawesome is a customized JavaScript testing reporter for mocha. 
# Mochawesome runs on Node. js (>=10) and works in conjunction with mochawesome-report-generator to generate 
# Standalone HTML/CSS reports to help visualize test runs.

# For instance, we can use the HTMLTestRunner package in Python to generate HTML test results for a test suite. 
# We can modify the previous script to use HTMLTestRunner and include the TestAdditional and TestHappyPath tests

import unittest
import HtmlTestRunner
from additional_test import TestAdditional
from happy_path import TestHappyPath

if __name__ == '__main__':
    # Create a test suite
    suite = unittest.TestSuite()

    # Add the tests to the suite
    suite.addTest(unittest.makeSuite(TestAdditional))
    suite.addTest(unittest.makeSuite(TestHappyPath))

    # Run the test suite and output the results to an HTML report
    runner = HtmlTestRunner.HTMLTestRunner(output='test-reports/')
    runner.run(suite)

# This modified script uses HTMLTestRunner to generate an HTML report of the test results, which is saved in the test-reports/ directory.
# We can open this report in a web browser to view the results, which should include the results of the TestAdditional and TestHappyPath tests.




